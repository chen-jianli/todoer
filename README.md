## ToDoer

**欢迎邮件交流技术问题：1552883346@qq.com**

<img src="https://img.shields.io/badge/lang-Qml-44C07D"/>
<img src="https://img.shields.io/badge/version-Qt5.14-1182C2"/>
<img src="https://img.shields.io/badge/license-LGPL2.1-F38D30"/>

### 1 介绍
ToDoer是一个Windows平台的桌面便签软件，可以用来方便的记录日常待办事项。   

安装包：<a>https://gitee.com/chen-jianli/todoer/releases</a>

### 2 软件效果
 - **ToDo列表**  
 ![ToDo](src/images/todo.png)

 - **Done列表**  
 ![Done](src/images/done.png)

  - **设置**  
 ![Setting](src/images/setting.png)



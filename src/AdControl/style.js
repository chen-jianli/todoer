﻿//base color
var color_flatBlue =  "#20A0FF"
var color_flatGreen = "#18A689"
var color_flatYellow  = "#F8AC59"
var color_flatRed = "#EC4758"

//text color
var color_textBlack = "#2d2f33"
var color_textGray = "#606266"
var color_textLightgray = "#909399"

//background & border color
var color_borderGray = "#BDBEBF"
var color_bgGray = "#81808F"

//text size
var size_bodyText = 18
var size_titleText = 21
var string_textFamily = "微软雅黑"

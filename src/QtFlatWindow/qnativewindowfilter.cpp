﻿#include <windows.h>
#include <QCoreApplication>

#include "qnativewindowfilter.h"
#include "qnativewindowfilter_p.h"
#include "qnativewindowhelper_p.h"

// class QNativeWindowFilter

void QNativeWindowFilter::deliver(QWindow *window, QNativeWindowHelper *helper)
{
    Q_CHECK_PTR(window);

    if (!QNativeWindowFilterPrivate::instance) {
        QCoreApplication *appc = QCoreApplication::instance();
        if (appc) {
            auto filter = new QNativeWindowFilter();
            QNativeWindowFilterPrivate::instance.reset(filter);
            appc->installNativeEventFilter(filter);
        }
    }

    if (helper) {
        WId newId = window->winId();
        WId oldId = QNativeWindowFilterPrivate::windows.value(helper);
        if (newId != oldId) {
            QNativeWindowFilterPrivate::helpers.insert(newId, helper);
            QNativeWindowFilterPrivate::helpers.remove(oldId);
            QNativeWindowFilterPrivate::windows.insert(helper, newId);

            QNativeWindowFilterPrivate::winIds.insert(window, newId);
        }
    } else {
        WId oldId = QNativeWindowFilterPrivate::winIds.take(window);
        QNativeWindowHelper *helper = QNativeWindowFilterPrivate::helpers.take(oldId);
        QNativeWindowFilterPrivate::windows.remove(helper);
    }
}

bool QNativeWindowFilter::nativeEventFilter(const QByteArray &eventType,
                                           void *message, long *result)
{
    Q_UNUSED(eventType);

    LPMSG msg = reinterpret_cast<LPMSG>(message);
    if (auto h = QNativeWindowFilterPrivate::helpers.value(reinterpret_cast<WId>(msg->hwnd)))
        return h->nativeEventFilter(msg, result);

    return false;
}

// class QNativeWindowFilterPrivate

QScopedPointer<QNativeWindowFilter> QNativeWindowFilterPrivate::instance;

QHash<QNativeWindowHelper *, WId> QNativeWindowFilterPrivate::windows;
QHash<QWindow *, WId> QNativeWindowFilterPrivate::winIds;
QHash<WId, QNativeWindowHelper *> QNativeWindowFilterPrivate::helpers;

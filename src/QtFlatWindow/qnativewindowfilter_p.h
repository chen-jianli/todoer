﻿#ifndef NATIVEWINDOWFILTER_P_H
#define NATIVEWINDOWFILTER_P_H

#include <QHash>
#include <QWindow>

#include "qnativewindowfilter.h"

class QNativeWindowFilterPrivate
{
public:
    static QScopedPointer<QNativeWindowFilter> instance;

    static QHash<QNativeWindowHelper *, WId> windows;
    static QHash<WId, QNativeWindowHelper *> helpers;
    static QHash<QWindow *, WId> winIds;
};

#endif // NATIVEWINDOWFILTER_P_H

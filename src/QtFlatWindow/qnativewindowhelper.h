﻿#ifndef NATIVEWINDOWHELPER_H
#define NATIVEWINDOWHELPER_H

#include <QPoint>
#include <QWindow>
#include <QMargins>

class NativeWindowTester
{
public:
    virtual QMargins draggableMargins() const = 0;
    virtual QMargins maximizedMargins() const = 0;

    virtual bool hitTest(const QPoint &pos) const = 0;
};

class QNativeWindowHelperPrivate;
class QNativeWindowHelper : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QNativeWindowHelper)

public:
    QNativeWindowHelper(QWindow *window, NativeWindowTester *tester);
    explicit QNativeWindowHelper(QWindow *window);
    ~QNativeWindowHelper();

    bool nativeEventFilter(void *msg, long *result);
    qreal scaleFactor() const;

    bool enableDrag();
    void setEnableDrag(bool enable);
    bool enableMaximize();
    void setEnableMaximize(bool enable);

protected:
    bool eventFilter(QObject *obj, QEvent *ev) final;
    QScopedPointer<QNativeWindowHelperPrivate> d_ptr;

signals:
    void scaleFactorChanged(qreal factor);

};

#endif // NATIVEWINDOWHELPER_H

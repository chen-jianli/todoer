﻿#ifndef QQuickFlatWindowHandler_H
#define QQuickFlatWindowHandler_H

#include <QQuickItem>
#include <QQmlParserStatus>

class QQuickFlatWindowHandlerPrivate;
class QQuickFlatWindowHandler : public QObject, public QQmlParserStatus
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QQuickFlatWindowHandler)

    Q_PROPERTY(int topDraggableMargin READ topDraggableMargin WRITE setTopDraggableMargin NOTIFY topDraggableMarginChanged)
    Q_PROPERTY(int leftDraggableMargin READ leftDraggableMargin WRITE setLeftDraggableMargin NOTIFY leftDraggableMarginChanged)
    Q_PROPERTY(int rightDraggableMargin READ rightDraggableMargin WRITE setRightDraggableMargin NOTIFY rightDraggableMarginChanged)
    Q_PROPERTY(int bottomDraggableMargin READ bottomDraggableMargin WRITE setBottomDraggableMargin NOTIFY bottomDraggableMarginChanged)

    Q_PROPERTY(int topMaximizedMargin READ topMaximizedMargin WRITE setTopMaximizedMargin NOTIFY topMaximizedMarginChanged)
    Q_PROPERTY(int leftMaximizedMargin READ leftMaximizedMargin WRITE setLeftMaximizedMargin NOTIFY leftMaximizedMarginChanged)
    Q_PROPERTY(int rightMaximizedMargin READ rightMaximizedMargin WRITE setRightMaximizedMargin NOTIFY rightMaximizedMarginChanged)
    Q_PROPERTY(int bottomMaximizedMargin READ bottomMaximizedMargin WRITE setBottomMaximizedMargin NOTIFY bottomMaximizedMarginChanged)

    Q_PROPERTY(int dragableHeight READ dragableHeight WRITE setDragableHeight NOTIFY dragableHeightChanged)
    Q_PROPERTY(qreal scaleFactor READ scaleFactor NOTIFY scaleFactorChanged)
    Q_PROPERTY(bool enableDrag READ enableDrag WRITE setEnableDrag NOTIFY enableDragChanged)
    Q_PROPERTY(bool enableMaximize READ enableMaximize WRITE setEnableMaximize NOTIFY enableMaximizeChanged)

    Q_INTERFACES(QQmlParserStatus)

public:
    explicit QQuickFlatWindowHandler(QObject *parent = nullptr);
    virtual ~QQuickFlatWindowHandler();
    void classBegin() final;
    void componentComplete() final;

signals:
    void topDraggableMarginChanged();
    void leftDraggableMarginChanged();
    void rightDraggableMarginChanged();
    void bottomDraggableMarginChanged();

    void topMaximizedMarginChanged();
    void leftMaximizedMarginChanged();
    void rightMaximizedMarginChanged();
    void bottomMaximizedMarginChanged();

    void dragableHeightChanged();
    void scaleFactorChanged();
    void enableDragChanged();
    void enableMaximizeChanged();

public slots:
    void addIncludeItem(QQuickItem *item);
    void removeIncludeItem(QQuickItem *item);
    void addExcludeItem(QQuickItem *item);
    void removeExcludeItem(QQuickItem *item);
    void addExcludeRect(QRect rect);

    void setDragableHeight(int value);

    void minimizeWindow();
    void maximizeWindow();
    void closeWindow();

public:
    void setBothDraggableMargin(int dm);
    void setTopDraggableMargin(int dm);
    void setLeftDraggableMargin(int dm);
    void setRightDraggableMargin(int dm);
    void setBottomDraggableMargin(int dm);
    int topDraggableMargin() const;
    int leftDraggableMargin() const;
    int rightDraggableMargin() const;
    int bottomDraggableMargin() const;

    void setBothMaximizedMargin(int dm);
    void setTopMaximizedMargin(int dm);
    void setLeftMaximizedMargin(int dm);
    void setRightMaximizedMargin(int dm);
    void setBottomMaximizedMargin(int dm);
    int topMaximizedMargin() const;
    int leftMaximizedMargin() const;
    int rightMaximizedMargin() const;
    int bottomMaximizedMargin() const;

    bool enableDrag() const;
    void setEnableDrag(bool enable);
    bool enableMaximize() const;
    void setEnableMaximize(bool enable);

    int dragableHeight() const;
    qreal scaleFactor() const;

protected:
    QScopedPointer<QQuickFlatWindowHandlerPrivate> d_ptr;
};

#endif // QQuickFlatWindowHandler_H

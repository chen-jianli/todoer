﻿#ifndef QSHARKFRAMELESSHELPER_P_H
#define QSHARKFRAMELESSHELPER_P_H

#include <QSet>
#include <QQuickWindow>
#include "qquickflatwindowhandler.h"
#include "qnativewindowhelper.h"

class QQuickFlatWindowHandlerPrivate : public NativeWindowTester
{
public:
    QQuickFlatWindowHandlerPrivate();
    virtual ~QQuickFlatWindowHandlerPrivate();

public:
    QMargins draggableMargins() const final;
    QMargins maximizedMargins() const final;

    bool hitTest(const QPoint &pos) const final;

public:
    QQuickWindow       *window;
    QNativeWindowHelper *helper;

    QMargins priDraggableMargins;
    QMargins priMaximizedMargins;
    QSet<QQuickItem *> extraTitleBars;
    QSet<QQuickItem *> includeItems;
    QSet<QQuickItem *> excludeItems;

    int dragableHeight;
    QVector<QRect>  excludeRects;
};

#endif // QSHARKFRAMELESSHELPER_P_H

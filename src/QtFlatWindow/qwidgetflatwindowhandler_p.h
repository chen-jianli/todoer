﻿#ifndef STYLEDHELPER_P_H
#define STYLEDHELPER_P_H

#include <QSet>

#include "qwidgetflatwindowhandler.h"

#include "qnativewindowhelper.h"

class QWidgetFlatWindowHandlerPrivate : public NativeWindowTester
{
public:
    QWidgetFlatWindowHandlerPrivate();
    virtual ~QWidgetFlatWindowHandlerPrivate();

public:
    QMargins draggableMargins() const final;
    QMargins maximizedMargins() const final;

    bool hitTest(const QPoint &pos) const final;

public:
    QWidget            *window;
    QNativeWindowHelper *helper;
public:
    QMargins priDraggableMargins;
    QMargins priMaximizedMargins;
public:
    QSet<QWidget *> extraTitleBars;
    int             titleBarHeight;
public:
    QSet<QWidget *> includeItems;
    QSet<QWidget *> excludeItems;

public:
    bool maximized;
};

#endif // STYLEDHELPER_P_H

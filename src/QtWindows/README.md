## QtWindows

### 1 介绍
QtWindows是一个基于Win32 API封装的Qt C++模块，其实现了Windows系统中许多有趣且常用的功能，其包括获取主机和用户信息、创建文件与应用程序关联、设置软件开机启动、读写系统环境变量、发送Windows窗口消息、监控系统CPU内存等硬件状态等等。

### 2 用法
（1）将QtWindows模块的整个文件夹复制到项目的某个目录下。  

（2）在项目的.pro文件中使用下述命令导入模块的.pri文件：
```
# ../QtWindows/qtwindows.pri是相对.pro文件的路径
include(../QtWindows/qtwindows.pri)
```

（3）对项目执行qmake刷新文件依赖。

### 3 API说明
#### 3.1 QWindowsManager
此类主要用于对Windows系统进行管理，其功能包括：
- 获取主机名称、IP、MAC地址等信息。
- 操作注册表来设置软件自启、创建文件关联、添加右键菜单、读写环境变量等。
- 同步或异步发送windows窗口消息。
- 获取文件的图标。
- 杀死指定名称或PID的进程。
- 调用本地默认程序打开文件或路径。

#### 3.2 QWindowsPerformance
此类主要用于对Windows的硬件状态进行监控，其功能包括：
- 同步阻塞式获取CPU使用率、内存使用率、网络上下行速度等。
- 基于信号槽异步获取CPU使用率、内存使用率、网络上下行速度等。

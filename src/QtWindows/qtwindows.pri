QT += core network

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += $$PWD\*.h

SOURCES += $$PWD\*.cpp

LIBS += User32.lib
LIBS += Kernel32.lib
LIBS += Advapi32.lib
LIBS += Shell32.lib
LIBS += Pdh.lib


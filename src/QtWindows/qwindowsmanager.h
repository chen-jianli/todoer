﻿#ifndef QWindowsManager_H
#define QWindowsManager_H

#include <QNetworkInterface>
#include <QIcon>

class QWindowsManager
{

public:

    //System Infomation
    static QString hostName();
    static QString userName();
    static QString wanIp();
    static QString lanIp();
    static QString mac();
    static QNetworkInterface activeNetworkInterface();

    //Registry Operation
    static bool createFileAssociation(QString extension,QString progid,bool cover=true);
    static bool createFileAssociation(QString extension,QString progid,QString description,QString iconPath,QString openCmd, bool cover=true);
    static bool createContextMenuItem(QString itemId,QString description,QString iconPath,QString openCmd,bool cover=true);
    static void removeContextMenuItem(QString itemId);
    static bool autoStartEnabled(QString appName);
    static void setAutoStartEnabled(QString appName,QString openCmd,bool on=true);
    static QStringList environmentVariableValue(QString name);
    static void setEnvironmentVariableValue(QString name, QStringList values);
    static void appendEnvironmentVariableValue(QString name, QStringList values);

    //Windows Message
    static long sendMessage(QString windowClassName,QString windowTitleName,unsigned int msgId,void *data);
    static bool postMessage(QString windowClassName,QString windowTitleName,unsigned int msgId,void *data);

    //Others
    static QIcon getFileIcon(QString filePath);
    static QIcon getFileIconWithExtension(QString fileExtension);

    static bool  killProcess(QString processName);
    static bool  killProcess(int processId);

    static bool  openUrl(QUrl url);

    static QString windowsErrorCodeToMessage(int errorCode);

};

#endif // QWindowsManager_H

﻿#ifndef QWindowsPerformance_H
#define QWindowsPerformance_H

#include <QObject>
#include <QThread>
#include <QSet>
#include <QJsonObject>

class QWindowsPerformance : public QThread
{
    Q_OBJECT

public:
    enum PerformanceType{
        CPUUsageRate=0,          // %
        CPURunTime=1,            // s
        MemoryUsageRate=2,       // %
        MemoryUsageAmount=3,     // mb
        NetworkDownloadSpeed=4,  // kb/s
        NetworkUploadSpeed=5,    // kb/s
    };

public:
    explicit QWindowsPerformance(QObject *parent = nullptr);
    ~QWindowsPerformance();

    //Sync Performance Monitor
    double performance(PerformanceType type);

    //Async Performance Monitor
    QSet<PerformanceType> monitoringPerformances();
    void addMonitoringPerformance(PerformanceType type);
    void clearMonitoringPerformance();

    bool startMonitor();
    bool reStartMonitor();
    bool stopMonitor();

    //Setting
    int  dataCollectionInterval();
    void setDataCollectionInterval(int interval=1000);

signals:
    //Emitted when new performance data is available to read
    void performanceReadReady(QJsonObject data);

    //Emitted when asynchronous monitoring fails
    void errorOccurred(QString errorMsg);

protected:
    void run() override;

private:
    bool runFlag;
    int m_dataCollectionInterval;
    QSet<PerformanceType> m_monitoringPerformances;
};

#endif // QWindowsPerformance_H

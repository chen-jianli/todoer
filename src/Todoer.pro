# 桌面便签软件
# Qt: Qt 5.14 MSVC
# OS: Windows 10/11
# Author: https://gitee.com/chen-jianli

#依赖配置
QT += quick quickcontrols2 widgets

#编译配置
CONFIG += c++11
DEFINES += QT_DEPRECATED_WARNINGS
DESTDIR = $$PWD/../target
CONFIG(debug, debug|release){
    TARGET = ToDoerD
} else {
    TARGET = ToDoer
}
msvc {
    QMAKE_CFLAGS += /utf-8
    QMAKE_CXXFLAGS += /utf-8
    QMAKE_CXXFLAGS += /wd4100
}

#资源配置
QML_IMPORT_PATH += ./
QML_DESIGNER_IMPORT_PATH =
RESOURCES += qml.qrc
RC_ICONS = app.ico

#子模块
include(./QtFlatWindow/qtflatwindow.pri)
include(./QtWindows/qtwindows.pri)

#源文件
SOURCES += \
        api/apiprovider.cpp \
        main.cpp

HEADERS += \
        api/apiprovider.h


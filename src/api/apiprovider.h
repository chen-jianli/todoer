﻿#ifndef APIPROVIDER_H
#define APIPROVIDER_H

#include <QObject>
#include <QJsonArray>
#include <QJsonObject>
#include <QDir>
#include <QQmlEngine>
#include <QQmlContext>
#include <QQuickWindow>
#include <QQmlComponent>

class ApiProvider : public QObject
{
    Q_OBJECT
public:
    explicit ApiProvider(QObject *parent = nullptr);

public slots:
    void loadWindow();
    void addWindow();
    void addWindow(const QString& configId);
    void removeWindow(const QString& configId);
    void closeAllWindow();

    QVector<QString> getConfigIds();

    QJsonArray readList(const QString& id,const QString& name);
    void saveList(const QString& id, const QString& name,const QJsonArray& items);

    QJsonObject readAppConfig(const QString& id);
    void saveAppConfig(const QString& id,const QJsonObject& obj);

private:
    bool removeDirectory(const QString &dirPath);

private:
    QString m_appPath;
    QString m_appName;
    QString m_appBaseName;
    QString m_configPath;

    QString m_qmlMainFilePath;
    QQmlComponent* m_qmlComponent;
    QQmlEngine* m_qmlEngine;
    QVector<QQuickWindow*> m_qmlWindows;
};

#endif

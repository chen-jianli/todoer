﻿#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QSGRendererInterface>
#include <QFont>
#include <QQuickStyle>
#include <QQmlContext>
#include <QScreen>
#include <QtMath>
#include <QQuickView>
#include <QUuid>
#include "qquickflatwindowhandler.h"
#include "api/apiprovider.h"

int main(int argc, char *argv[])
{
    //QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication::setApplicationName("ToDoer");
    QApplication::setQuitOnLastWindowClosed(false);

    QApplication app(argc, argv);
    QFont font;
    font.setFamily("Microsoft YaHei");
    app.setFont(font);

    QQuickWindow::setSceneGraphBackend(QSGRendererInterface::Software);
    QQuickStyle::setStyle("Fusion");
    qmlRegisterType<QQuickFlatWindowHandler>("Qt.FlatWindow", 1, 0, "QQuickFlatWindowHandler");

    ApiProvider api;
    api.loadWindow();

    return app.exec();
}

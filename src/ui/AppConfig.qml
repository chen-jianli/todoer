﻿import QtQuick 2.0

Item {
    property bool stayOnTop:false
    property bool lockPosition:false
    property bool autoStart:true
    property int itemTextSize: 25
    property color itemTextColor: "white"
    property real bgOpacity:0.6
    property color bgColor:Qt.rgba(0,0,0,1)
    property int autoSaveIntervel: 60*1000

    property alias autoSaveTimer:autoAppConfigTimer

    property var __lastWindowPos: {
        "x":-1,
        "y":-1,
        "width":-1,
        "height":-1
    }
    property bool __initOk:false

    visible: false

    onBgColorChanged: {
        if(__initOk){
           autoAppConfigTimer.restart();
        }
    }

    onBgOpacityChanged: {
        if(__initOk){
           autoAppConfigTimer.restart();
        }
    }

    onAutoSaveIntervelChanged: {
        if(__initOk){
           autoAppConfigTimer.restart();
        }
    }

    onStayOnTopChanged: {
        if(stayOnTop){
            rootWindow.flags =  Qt.SubWindow | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint
        }
        else{
            rootWindow.flags =  Qt.SubWindow | Qt.FramelessWindowHint
        }

        if(__initOk){
           autoAppConfigTimer.restart();
        }
    }

    onLockPositionChanged: {
        flatWindowHandler.enableDrag = !lockPosition;
        if(__initOk){
            if(lockPosition){
                __lastWindowPos.x = rootWindow.x;
                __lastWindowPos.y = rootWindow.y;
                __lastWindowPos.width = rootWindow.width;
                __lastWindowPos.height = rootWindow.height;
            }
            autoAppConfigTimer.restart();
        }
    }

    onAutoStartChanged: {
        if(__initOk){
           autoAppConfigTimer.restart();
        }
    }

    onItemTextSizeChanged: {
        itemView.textSize = itemTextSize

        if(__initOk){
           autoAppConfigTimer.restart();
        }
    }

    onItemTextColorChanged: {
        itemView.textColor = itemTextColor

        if(__initOk){
           autoAppConfigTimer.restart();
        }
    }

    Timer{
        id:autoAppConfigTimer
        interval :autoSaveIntervel
        onTriggered:  {
           saveAppConfigToFile()
        }
    }

    function initConfig(appSetting){
        for(var i in appSetting){
            if(i === "stayOnTop"){
                stayOnTop = appSetting[i]
            }
            else if(i === "lockPosition"){
                lockPosition = appSetting[i]
            }
            else if(i === "autoStart"){
                autoStart = appSetting[i]
            }
            else if(i === "itemTextSize"){
                itemTextSize = appSetting[i]
            }
            else if(i === "itemTextColor"){
                itemTextColor = appSetting[i]
            }
            else if(i === "bgColor"){
                bgColor = appSetting[i]
            }
            else if(i === "bgOpacity"){
                bgOpacity = appSetting[i]
            }
            else if(i === "autoSaveIntervel"){
                autoSaveIntervel = appSetting[i]
            }
            else if(i === "lastWindowPos"){
                __lastWindowPos = appSetting[i]
            }
        }

        __initOk = true;
    }

    function saveAppConfigToFile(){
        var appConfig = {
            "stayOnTop":stayOnTop,
            "lockPosition":lockPosition,
            "autoStart":autoStart,
            "itemTextSize":itemTextSize,
            "itemTextColor":String(itemTextColor),
            "bgColor":String(bgColor),
            "bgOpacity":bgOpacity,
            "autoSaveIntervel":autoSaveIntervel,
            "lastWindowPos":__lastWindowPos
        }

        ApiProvider.saveAppConfig(configId,appConfig)
    }
}

﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls 1.4 as Control1
import QtQuick.Layouts 1.12

Control1.TabView {
    id:rootControl

    property int textSize:26
    property color textColor:"white"
    property int autoSaveInterval: 60*1000
    property alias autoSaveTimer:autoSaveModelTimer
    property alias todoTab:todoTab
    property alias doneTab:doneTab

    property bool __todoListChanged:false
    property bool __doneListChanged:false

    tabsVisible: false
    frameVisible: false

    Control1.Tab{
        id:todoTab

        ListView{
            id:todoListView
            interactive: todoListView.contentHeight>rootControl.height
            anchors.fill: parent
            spacing: 10
            clip: true
            currentIndex: -1
            keyNavigationEnabled: true
            keyNavigationWraps:true
            model:todoListModel            
            delegate: Item{
                id:compItem
                width: parent.width
                height:itemText.text===""? textSize*1.2:itemText.lineCount*textSize*1.2

                property  bool  isDragBegin:false
                property  int   itemIndex:index
                property bool   isCurrentItem:ListView.isCurrentItem

                //行号
                Item{
                    id:dragItem
                    width: textSize*0.6
                    height: parent.height

                    Rectangle{
                        id:circleRect
                        anchors.top: parent.top
                        anchors.topMargin:textSize/2
                        width: textSize*0.5
                        height: textSize*0.5
                        radius: textSize*0.5/2
                        color: textColor
                    }
                }

                //文本
                TextEdit{
                    id:itemText
                    width: parent.width - dragItem.width - itemText.anchors.leftMargin
                    wrapMode: Text.Wrap
                    font.pixelSize: textSize
                    anchors.left: dragItem.right
                    anchors.leftMargin: textSize*0.5
                    color: textColor
                    text: contentText
                    selectByMouse: true
                    onCursorVisibleChanged: {
                        if(cursorVisible){
                            todoListView.currentIndex = itemIndex
                            todoListMenu.visible  = false
                        }
                    }
                    onEditingFinished: {
                        setItem("ToDo",itemIndex,"contentText",itemText.text)                        
                    }
                }

                //操作菜单
                ItemOperationMenu{
                    id:todoListMenu
                    height:itemText.height
                    z:100
                    buttons: [
                        {"name":"done","icon":"/icon/done.png","toolTip":""},
                        {"name":"delete","icon":"/icon/delete.png","toolTip":""}
                    ]

                    onButtonTrigger: {
                        if(name === "done"){
                            transferItem(itemIndex,"ToDo", "Done")
                            todoListView.currentIndex = -1
                        }
                        else if(name === "delete"){
                            deleteItem("ToDo",itemIndex)
                            todoListView.currentIndex = -1
                        }
                    }
                }

                //处理拖动
                MouseArea{
                    id: dragArea
                    anchors.fill: dragItem
                    onPressed:
                    {
                        isDragBegin = true
                        todoListView.interactive = false;
                    }
                    onReleased:
                    {
                        isDragBegin = false
                        todoListView.interactive = (todoListView.contentHeight>rootControl.height);
                    }
                    onMouseYChanged: {
                        if(isDragBegin)
                        {
                            var dstRowIndex = todoListView.indexAt(mouseX + compItem.x, mouseY + compItem.y);
                            if ((dstRowIndex < 0) || (dstRowIndex > todoListModel.rowCount()))
                                return ;
                            if (itemIndex !== dstRowIndex){
                                todoListModel.move(itemIndex,dstRowIndex,1);
                            }
                        }
                    }
                }

                //处理悬浮
                MouseArea{
                    id: menuArea
                    acceptedButtons:Qt.NoButton
                    anchors.fill: compItem
                    z:5
                    hoverEnabled:true
                    onContainsMouseChanged:{
                        if(!itemText.cursorVisible && menuArea.containsMouse){
                            todoListMenu.x = itemText.x + itemText.width -todoListMenu.width
                            todoListMenu.y = itemText.y
                            todoListMenu.visible = true
                        }
                        else{
                            todoListMenu.visible = false
                        }
                    }
                }

                //处理导航
                onIsCurrentItemChanged: {
                    if(isCurrentItem){
                        itemText.forceActiveFocus()
                    }
                    else{
                        itemText.cursorVisible = false
                    }
                }
            }
        }
    }

    Control1.Tab{
        id:doneTab

        ListView{
            id:doneListView
            interactive: doneListView.contentHeight>rootControl.height
            anchors.fill: parent
            currentIndex: -1
            spacing: 10
            clip: true
            model:doneListModel            
            delegate: Item{
                id:doneItem
                width: parent.width
                height:itemText2.text===""? textSize*1.2:itemText2.lineCount*textSize*1.2

                property  bool  isDragBegin:false
                property  int   itemIndex:index

                //行号
                Item{
                    id:dragItem2
                    width: textSize*0.6
                    height: parent.height

                    Rectangle{
                        id:circleRect2
                        anchors.top: parent.top
                        anchors.topMargin:textSize/2
                        width: textSize*0.5
                        height: textSize*0.5
                        radius: textSize*0.5/2
                        color: textColor
                    }
                }

                //文本
                TextEdit{
                    id:itemText2
                    width: parent.width - dragItem2.width - itemText2.anchors.leftMargin
                    wrapMode: Text.Wrap
                    font.pixelSize: textSize
                    anchors.left: dragItem2.right
                    anchors.leftMargin: textSize*0.5
                    color: textColor
                    text: contentText
                    selectByMouse: true;
                    readOnly: true
                }

                //操作菜单
                ItemOperationMenu{
                    id:doneListMenu
                    height:itemText2.height
                    buttons: [
                        {"name":"return","icon":"/icon/return.png","toolTip":""},
                        {"name":"delete","icon":"/icon/delete.png","toolTip":""}
                    ]

                    onButtonTrigger: {
                        if(name === "return"){
                            transferItem(itemIndex,"Done", "ToDo")
                        }
                        else if(name === "delete"){
                            deleteItem("Done",itemIndex)
                        }
                    }
                }

                //处理拖动
                MouseArea{
                    id: dragArea2
                    anchors.fill: dragItem2
                    onPressed:
                    {
                        isDragBegin = true
                        doneListView.interactive = false;
                    }
                    onReleased:
                    {
                        isDragBegin = false
                        doneListView.interactive = (doneListView.contentHeight>rootControl.height);
                    }
                    onMouseYChanged: {
                        if(isDragBegin)
                        {
                            var dstRowIndex = doneListView.indexAt(mouseX + doneItem.x, mouseY + doneItem.y);
                            if ((dstRowIndex < 0) || (dstRowIndex > doneListModel.rowCount()))
                                return ;
                            if (itemIndex !== dstRowIndex){
                                doneListModel.move(itemIndex,dstRowIndex,1);
                            }
                        }
                    }
                }

                //处理悬浮
                MouseArea{
                    id: menuArea2
                    acceptedButtons:Qt.NoButton
                    anchors.fill: doneItem
                    z:5
                    hoverEnabled:true
                    onContainsMouseChanged:{
                        if(!itemText2.cursorVisible && menuArea2.containsMouse){
                            doneListMenu.x = itemText2.x + itemText2.width -doneListMenu.width
                            doneListMenu.y = itemText2.y
                            doneListMenu.visible = true
                        }
                        else{
                            doneListMenu.visible = false
                        }
                    }
                }
            }

        }
    }

    ListModel{
        id:todoListModel
    }

    ListModel{
        id:doneListModel
    }

    Timer{
        id:autoSaveModelTimer
        interval: autoSaveInterval
        onTriggered: {
            saveModelToFile()
        }
    }

    function initModel(modelName,model){

        if(modelName === "ToDo")
        {
            todoListModel.clear()

            for(var i in model)
            {
                var data= model[i];
                todoListModel.append(data);
            }
        }
        else if(modelName === "Done")
        {
            doneListModel.clear()

            for(var i2 in model)
            {
                var data2= model[i2];
                doneListModel.append(data2);
            }
        }
    }

    function newItem(modelName){
        if(modelName === "ToDo")
        {
            var item = {"contentText":""}
            todoListModel.append(item)
            __todoListChanged = true
            autoSaveModelTimer.restart()
        }
    }

    function deleteItem(modelNmae,modelIndex){

        if(modelNmae === "ToDo")
        {
            todoListModel.remove(modelIndex)
            __todoListChanged = true
            autoSaveModelTimer.restart()
        }
        else if(modelNmae === "Done")
        {
            doneListModel.remove(modelIndex)
            __doneListChanged = true
            autoSaveModelTimer.restart()
        }
    }

    function transferItem(fromModelIndex,fromModelName,dstModelName){
        if(fromModelName === "ToDo" && dstModelName === "Done")
        {
            var obj = todoListModel.get(fromModelIndex)
            doneListModel.insert(0,obj)
            todoListModel.remove(fromModelIndex)
            __todoListChanged = true
            __doneListChanged = true
            autoSaveModelTimer.restart()
        }
        else if(fromModelName === "Done" && dstModelName === "ToDo"){
            var obj2 = doneListModel.get(fromModelIndex)
            todoListModel.insert(0,obj2)
            doneListModel.remove(fromModelIndex)
            __todoListChanged = true
            __doneListChanged = true
            autoSaveModelTimer.restart()
        }
    }

    function setItem(modelName,modelIndex,propName,propValue){
        if(modelName === "ToDo")
        {
            todoListModel.setProperty(modelIndex,propName,propValue)
            __todoListChanged = true
            autoSaveModelTimer.restart()
        }
        else if(modelName === "Done")
        {
            doneListModel.setProperty(modelIndex,propName,propValue)
            __doneListChanged = true
            autoSaveModelTimer.restart()
        }
    }

    function saveModelToFile(){
        if(__todoListChanged){
            var model1 = []
            for(var i=0;i<todoListModel.count;i++)
            {
                model1.push(todoListModel.get(i))
            }

            ApiProvider.saveList(configId,"ToDo",model1);
            __todoListChanged = false;
        }
        if(__doneListChanged){
            var model2 = []
            for(var i2=0;i2<doneListModel.count;i2++)
            {
                model2.push(doneListModel.get(i2))
            }

            ApiProvider.saveList(configId,"Done",model2);
            __doneListChanged = false;
        }
    }
}

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Rectangle {
    id:rootControl

    property int iconSize:26
    property var buttons:[]
    property bool containMouse:false

    width: row.width
    visible:  false
    color: "transparent"

    signal buttonTrigger(var name)

    Row{
        id:row
        anchors.verticalCenter: parent.verticalCenter
        spacing: 5
        Repeater{
            id:repeater
            model: buttons
            Image{
                id:iconButton
                width:iconSize
                height:iconSize
                source:modelData.icon

                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        buttonTrigger(modelData.name)
                        mouse.accepted = true
                    }
                }
            }
        }
    }
}

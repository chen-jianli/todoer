﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import AdControl 1.0
import Qt.labs.platform 1.1

Window {
    id:settingWindow
    width: 600
    height: 400
    flags: Qt.Window | Qt.FramelessWindowHint
    visible: false
    color:{
        var r = appConfig.bgColor.r
        var g = appConfig.bgColor.g
        var b = appConfig.bgColor.b

        return Qt.rgba(r,g,b,appConfig.bgOpacity)
    }

    Image{
        source: "/icon/close.png"
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        width: 32
        height: 32

        MouseArea{
            anchors.fill: parent
            onClicked: {
                settingWindow.close()
            }
        }
    }

    Item{
        width: parent.width-80
        height: parent.height-100
        anchors.centerIn: parent

        Column{
            spacing: 25

            Row{
                spacing: 20
                AdSwitchButton {
                    id:autoStartSwitch
                    text:"开机自启"
                    buttonTextCheckedColor:"white"
                    buttonTextUnCheckedColor:"white"

                    onCheckedChanged: {
                        appConfig.autoStart = checked
                    }
                }

                AdSwitchButton {
                    id:stayOnTopSwitch
                    text:"窗口置顶"
                    buttonTextCheckedColor:"white"
                    buttonTextUnCheckedColor:"white"

                    onCheckedChanged: {
                        appConfig.stayOnTop = checked
                    }
                }
            }

            Row{
                Rectangle{                    
                    id:textColorRect
                    width:60
                    height:30          

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            colorDialog.show("字体颜色",textColorRect.color);
                        }
                    }
                }
                Item{
                    width: 5
                    height: 20
                }
                Text{
                    text:"字体颜色"
                    color: "white"
                    font.pixelSize: 18
                }
                Item{
                    width: 35
                    height: 20
                }
                Rectangle{
                    id:bgColorRect
                    width:60
                    height:30
                    color: "black"

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            colorDialog.show("背景颜色",bgColorRect.color);
                        }
                    }
                }
                Item{
                    width: 5
                    height: 20
                }
                Text{
                    color: "white"
                    text:"背景颜色"
                    font.pixelSize: 18
                }
            }

            Row{
                spacing: 10
                Text{
                    color: "white"
                    text:"字体大小"
                    font.pixelSize: 18
                }

                AdNumberInput{
                    id:textSizeInput
                    from: 15
                    to:50
                    value: 25
                    width: 100
                    height: 30
                    font.pixelSize:18
                    inputTextNormalColor:"white"
                    inputBorderNormalColor:"white"
                    inputBorderActiveColor:"white"

                    onValueModified: {
                        appConfig.itemTextSize = value
                    }
                }
            }

            Row{
                spacing: 10
                Text{
                    color: "white"
                    text:"背景透明度"
                    font.pixelSize: 18
                }

                Slider{
                    id:bgOpacitySlider
                    width: 150
                    height: 30
                    from: 0.1
                    to: 1
                    value: 0.5

                    onMoved:{
                        appConfig.bgOpacity = value
                    }
                }
            }
        }
    }

    Text{
        id:linkText

        property string url: "https://gitee.com/chen-jianli"

        anchors.right:  parent.right
        anchors.bottom: parent.bottom
        anchors.margins:10
        padding: 0
        font.pixelSize: 18
        font.underline: true
        text:"作者: "+url
        color:linkTextMouseArea.containsMouse?"#20A0FF":"white"
        MouseArea{
            id:linkTextMouseArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                Qt.openUrlExternally(linkText.url)
            }
        }
    }

    ColorDialog{
        id:colorDialog
        property string uid;

        onAccepted: {
            if(uid == "背景颜色"){
                bgColorRect.color = colorDialog.color;
                appConfig.bgColor = colorDialog.color;
            }
            else if(uid == "字体颜色"){
                textColorRect.color = colorDialog.color;
                appConfig.itemTextColor = colorDialog.color;
            }
        }

        function show(userId,oldColor){
            uid = userId;
            colorDialog.currentColor = oldColor
            colorDialog.open();
        }
    }

    onVisibleChanged: {
        if(visible){
            autoStartSwitch.checked = appConfig.autoStart
            stayOnTopSwitch.checked = appConfig.stayOnTop
            textColorRect.color = appConfig.itemTextColor
            bgColorRect.color = appConfig.bgColor
            bgOpacitySlider.value = appConfig.bgOpacity
            textSizeInput.value = appConfig.itemTextSize
        }
    }
}

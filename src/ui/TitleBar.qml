﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Rectangle {
    id:rootControl

    property alias tabButtonGroup:flow
    property var tabButtons:[]
    property int tabButtonMaxSize:50

    property alias iconButtonGroup:row
    property var iconButtons:[]
    property int iconButtonSize:28

    signal buttonTrigger(var name)

    color: "transparent"
    clip: true

    onTabButtonsChanged: {
        repeater.model = tabButtons
    }

    onIconButtonsChanged: {
        repeater2.model = iconButtons
    }

    Flow {
        id:flow
        spacing: 20
        Repeater {
            id:repeater
            model: rootControl.tabButtons

            Item{
                id: button
                height:tabButtonMaxSize
                width:buttonText.contentWidth

                Text {
                    id:buttonText
                    color: button.Positioner.isFirstItem?appConfig.itemTextColor:Qt.darker(appConfig.itemTextColor,1.4)
                    font.pixelSize:button.Positioner.isFirstItem? tabButtonMaxSize*0.8:tabButtonMaxSize*0.5
                    font.bold: true
                    text: modelData
                    anchors.bottom: button.bottom
                    anchors.bottomMargin:button.Positioner.isFirstItem? 0:2.5
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                MouseArea{
                    anchors.fill: parent
                    onClicked:{
                        rootControl.buttonTrigger(buttonText.text);

                        for(var i in tabButtons)
                        {
                            if(tabButtons[i] === buttonText.text)
                            {
                                tabButtons.splice(i,1)
                                break;
                            }
                        }
                        tabButtons.unshift(buttonText.text)

                        tabButtonsChanged()
                    }
                }
            }
        }
    }

    Item{
        id:row
        width: parent.width*0.3
        height:parent.height
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter

        RowLayout{
            height: parent.height
            anchors.right: parent.right
            id:iconButtonLayout
            visible: false
            spacing: 10

            Item{
                Layout.fillWidth: true
            }

            Repeater{
                id:repeater2
                model: iconButtons

                Image{
                    id:iconButton
                    Layout.preferredWidth: iconButtonSize
                    Layout.preferredHeight: iconButtonSize
                    clip: true
                    source:modelData.icon

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            buttonTrigger(modelData.name)
                        }
                    }
                }
            }
        }

        MouseArea{
            id:iconButtonHoverdArea
            acceptedButtons: Qt.NoButton
            anchors.fill: parent
            hoverEnabled: true
            onContainsMouseChanged: {
                if(containsMouse){
                    iconButtonLayout.visible = true
                }
                else{
                    iconButtonLayout.visible = false
                }
            }
        }
    }
}

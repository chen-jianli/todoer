﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Qt.labs.platform 1.1

SystemTrayIcon {
    id:rootControl
    tooltip:"ToDoer"
    visible: true
    icon.source: "/icon/app.png"

    signal buttonTrigger(var name)

    menu: Menu{
        font.pixelSize:20
        minimumWidth: 120

        MenuItem{
            text:"新建"
            onTriggered: {
               buttonTrigger(text)
            }
        }

        MenuItem{
            text:"删除"
            onTriggered: {
               buttonTrigger(text)
            }
        }

        MenuItem{
            text:"隐藏"
            onTriggered: {
               buttonTrigger(text)
            }
        }
        MenuItem{
            text:"设置"
            onTriggered: {
               buttonTrigger(text)
            }
        }
        MenuItem{
            text:"重置位置"
            onTriggered: {
               buttonTrigger(text)
            }
        }
        MenuSeparator{}
        MenuItem{
            text:"退出"
            onTriggered: {
               buttonTrigger(text)
            }
        }
    }

    onActivated: {
        if(reason === SystemTrayIcon.Trigger){
            buttonTrigger("激活");
        }
        else if(reason === SystemTrayIcon.Context){
            rootControl.menu.open()
        }
    }
}

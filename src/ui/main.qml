﻿import QtQuick 2.12
import QtQuick.Window 2.12
import Qt.FlatWindow 1.0

Window {    
    id:rootWindow
    x:0
    y:0
    width: 400
    height: 600
    flags: Qt.SubWindow | Qt.FramelessWindowHint
    visible: true
    minimumWidth: titleBar.tabButtonGroup.width + titleBar.iconButtonGroup.width +50
    minimumHeight: titleBar.tabButtonMaxSize+200
    color:{
        var r = appConfig.bgColor.r
        var g = appConfig.bgColor.g
        var b = appConfig.bgColor.b

        return Qt.rgba(r,g,b,appConfig.bgOpacity)
    }

    property bool isRemoved: false

    AppConfig{
        id: appConfig
    }

    TrayIcon{
        id:trayIcon

        onButtonTrigger: {
            if(name === "激活"){
                if(rootWindow.visible){
                    rootWindow.visible=false
                }else{
                    rootWindow.visible = true
                    rootWindow.raise()
                }
            }
            else if(name === "新建"){
                ApiProvider.addWindow()
            }
            else if(name === "删除"){
                isRemoved=true;
                ApiProvider.removeWindow(configId)
            }
            else if(name === "隐藏"){
                rootWindow.visible = false
            }
            else if(name === "设置"){
                settingDialg.show();
                settingDialg.raise();
            }
            else if(name === "重置位置"){
                var x = Screen.width/2 - rootWindow.width/2
                var y = Screen.height/2 - rootWindow.height/2;
                rootWindow.x=x;
                rootWindow.y=y;

                appConfig.autoSaveTimer.restart();
            }
            else if(name === "退出"){

                Qt.quit()
            }
        }
    }

    QQuickFlatWindowHandler{
        id:flatWindowHandler

        leftMaximizedMargin: 0
        rightMaximizedMargin: 0
        topMaximizedMargin: 0
        bottomMaximizedMargin: 0

        topDraggableMargin: 5
        leftDraggableMargin: 5
        rightDraggableMargin: 5
        bottomDraggableMargin: 5

        dragableHeight: titleBar.height
    }

    TitleBar{
        id:titleBar
        anchors.top:parent.top
        anchors.topMargin:20
        anchors.left:parent.left
        anchors.leftMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width-titleBar.anchors.leftMargin - 10
        height: titleBar.tabButtonMaxSize
        tabButtons: ["ToDo","Done"]
        onButtonTrigger:
        {
            if(name==="ToDo"){
                itemView.currentIndex = 0;
                itemView.todoTab.item.currentIndex = -1
                if(appConfig.lockPosition){
                    iconButtons = [
                        {"name":"add","icon":"/icon/add.png"},
                        {"name":"lock","icon":"/icon/lock.png"},
                        {"name":"setting","icon":"/icon/setting.png"}
                    ]
                }
                else{
                    iconButtons = [
                        {"name":"add","icon":"/icon/add.png"},
                        {"name":"unlock","icon":"/icon/unlock.png"},
                        {"name":"setting","icon":"/icon/setting.png"}
                    ]
                }
            }
            else if(name==="Done")
            {
                itemView.currentIndex = 1;
                if(appConfig.lockPosition){
                    iconButtons = [
                        {"name":"lock","icon":"/icon/lock.png"},
                        {"name":"setting","icon":"/icon/setting.png"}
                    ]
                }
                else{
                    iconButtons = [
                        {"name":"unlock","icon":"/icon/unlock.png"},
                        {"name":"setting","icon":"/icon/setting.png"}
                    ]
                }
            }
            else if(name==="lock"){
                for(var i in iconButtons){
                    if(iconButtons[i].name === "lock"){
                        iconButtons[i].name = "unlock"
                        iconButtons[i].icon = "/icon/unlock.png"
                    }
                }
                titleBar.iconButtonsChanged()
                appConfig.lockPosition = false
            }
            else if(name==="unlock"){
                for(var i2 in iconButtons){
                    if(iconButtons[i2].name === "unlock"){
                        iconButtons[i2].name = "lock"
                        iconButtons[i2].icon = "/icon/lock.png"
                    }
                }
                titleBar.iconButtonsChanged()
                appConfig.lockPosition = true
            }
            else if (name === "add"){
                itemView.newItem("ToDo")
            }
            else if(name==="setting"){
                settingDialg.visible = true
            }
        }
    }

    ItemListView{
        id:itemView
        anchors.top:titleBar.bottom
        anchors.topMargin:20
        anchors.left:parent.left
        anchors.leftMargin: 30
        autoSaveInterval: appConfig.autoSaveIntervel
        textSize: appConfig.itemTextSize
        textColor: appConfig.itemTextColor
        width: parent.width-itemView.anchors.leftMargin - 10
        height: parent.height -titleBar.anchors.topMargin -titleBar.height -itemView.anchors.topMargin - 10
    }

    SettingDialog{
        id:settingDialg
        x: Screen.width/2 - settingDialg.width/2
        y: Screen.height/2 - settingDialg.height/2
    }

    Component.onCompleted:{
        flatWindowHandler.addExcludeItem(titleBar.tabButtonGroup)
        flatWindowHandler.addExcludeItem(titleBar.iconButtonGroup)

        //加载配置
        var appSetting = ApiProvider.readAppConfig(configId)
        appConfig.initConfig(appSetting)

        if (appConfig.__lastWindowPos.width !== -1 || appConfig.__lastWindowPos.height !== -1){
            rootWindow.width = appConfig.__lastWindowPos.width;
            rootWindow.height =  appConfig.__lastWindowPos.height;
        }
        if(appConfig.__lastWindowPos.x !== -1 || appConfig.__lastWindowPos.y !== -1){
            rootWindow.x = appConfig.__lastWindowPos.x;
            rootWindow.y =  appConfig.__lastWindowPos.y;
        }else{
            var x = Screen.width/2 - rootWindow.width/2
            var y = Screen.height/2 - rootWindow.height/2;
            rootWindow.x=x;
            rootWindow.y=y;
        }

        rootWindow.showNormal();

        if(appConfig.lockPosition){
            titleBar.iconButtons = [
                {"name":"lock","icon":"/icon/lock.png"},
                {"name":"add","icon":"/icon/add.png"},
                {"name":"setting","icon":"/icon/setting.png"}
            ]
        }else{
            titleBar.iconButtons = [
                {"name":"unlock","icon":"/icon/unlock.png"},
                {"name":"add","icon":"/icon/add.png"},
                {"name":"setting","icon":"/icon/setting.png"}
            ]
        }

        //加载数据
        var todoModel =ApiProvider.readList(configId,"ToDo");
        itemView.initModel("ToDo",todoModel)
        var doneModel =ApiProvider.readList(configId,"Done");
        itemView.initModel("Done",doneModel)
    }

    onClosing:{
        itemView.autoSaveTimer.stop()
        appConfig.autoSaveTimer.stop()

        if(!isRemoved){
            itemView.saveModelToFile();
            appConfig.saveAppConfigToFile()
        }

        close.accepted = true
    }
}
